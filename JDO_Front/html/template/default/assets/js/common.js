$(function () {

  //ハンバーガーメニュー
  var state = false;
  var scrollpos;
  $('.hamburger_icon').on('click', function () {
    $(this).toggleClass('open');
    $(".l-header .navi").fadeToggle();
    $(".sp_menu_bg").fadeToggle();
    if (state == false) {
      scrollpos = $(window).scrollTop();
      $('body').addClass('fixed').css({ 'top': -scrollpos });
      state = true;
    } else {
      $('body').removeClass('fixed').css({ 'top': 0 });
      window.scrollTo(0, scrollpos);
      state = false;
    }
    return false;
  });
  var w = $(window).width();
  var x = 799;
  //スマホ時のみ動作
  if (w <= x) {
    $('.menu li a').on('click', function () {
      $(".navi").fadeOut();
      $(".hamburger_icon").removeClass("open");
    });
  } else {  //PC時のみ動作
    //タブ切り替え
    $('.ChangeElem_Btn').each(function () {
      $(this).on('click', function () {
        var index = $('.ChangeElem_Btn').index(this);
        $('.ChangeElem_Btn').removeClass('active');
        $(this).addClass('active');
        $('.ChangeElem_Panel').removeClass('active');
        $('.ChangeElem_Panel').eq(index).addClass('active');
      });
    });
  }

  //別ページのアンカーリンク ?id=○○ で遷移
  $(window).on('load', function () {
    var url = $(location).attr('href');
    if (url.indexOf("?id=") != -1) {
      var id = url.split("?id=");
      var $target = $('#' + id[id.length - 1]);
      if ($target.length) {
        var pos = $target.offset().top;
        $("html, body").animate({ scrollTop: pos }, 1000);
      }
    }
  });

  $('.shop-bnr_slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
  });

  $('.main_coupon_area').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
  });

  $('.slider_area').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });

  $('.iyashi_slider_innner').slick({
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 6,

  });

  $('.new_face_group').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
  });

  $('.mv_img').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
  });
  $('.thum_img').slick({
    slidesToShow: 5,
    asNavFor: '.mv_img',
    autoplay: false,
    focusOnSelect: true,
    arrows: false,
    variableWidth: true,
  });
  // $('.photo_nav_slider').slick({
  //   infinite: true,
  //   speed: 300,
  //   slidesToShow: 1,
  //   adaptiveHeight: true
  // });
  $('.modal').modaal({
    custom_class: "modal_custom",
  });
  var slider = $('.photo_nav_slider').slick();
  $('.modal').click(function () {
    slider.slick('setPosition');
  });
  $('.close').click(function () {
    $('.modal').modaal("close");
  });
  if ($('#scroll_area').length) {
    var $fixElement = $('#scroll_area'); // 追従する要素
    var baseFixPoint = $fixElement.offset().top; // 追従する要素の初期位置
    var fixClass = 'is-fixed'; // 追従時に付与するclass

    // 要素が追従する処理
    function fixFunction() {
      var windowScrolltop = $(window).scrollTop();
      // スクロールが初期位置を通過しているとき
      if (windowScrolltop >= baseFixPoint) {
        $fixElement.addClass(fixClass);
      } else {
        $fixElement.removeClass(fixClass);
      }
    }
    $(window).on('load scroll', function () {
      fixFunction();
    });
  }

});
