$(function(){

  //ハンバーガーメニュー
  var state = false;
  var scrollpos;
  $('.hamburger_icon').on('click', function() {
    $(this).toggleClass('open');
    $(".l-header .navi").fadeToggle();
    $(".sp_menu_bg").fadeToggle();
    if(state == false) {
      scrollpos = $(window).scrollTop();
      $('body').addClass('fixed').css({'top': -scrollpos});
      state = true;
    } else {
      $('body').removeClass('fixed').css({'top': 0});
      window.scrollTo( 0 , scrollpos );
      state = false;
    }
    return false;
  });
  var w = $(window).width();
	var x = 799;
  //スマホ時のみ動作
	if (w <= x) {
    $('.menu li a').on('click', function() {
      $(".navi").fadeOut();
      $(".hamburger_icon").removeClass("open");
    });
  } else {  //PC時のみ動作
    //タブ切り替え
    $('.ChangeElem_Btn').each(function () {
      $(this).on('click', function () {
        var index = $('.ChangeElem_Btn').index(this);
        $('.ChangeElem_Btn').removeClass('active');
        $(this).addClass('active');
        $('.ChangeElem_Panel').removeClass('active');
        $('.ChangeElem_Panel').eq(index).addClass('active');
      });
    });
  }

  //別ページのアンカーリンク ?id=○○ で遷移
  $(window).on('load', function() {
    var url = $(location).attr('href');
    if(url.indexOf("?id=") != -1){
      var id = url.split("?id=");
      var $target = $('#' + id[id.length - 1]);
      if($target.length){
        var pos = $target.offset().top;
        $("html, body").animate({scrollTop:pos}, 1000);
      }
    }
  });

  $('.shop-bnr_slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
  });

  $('.main_coupon_area').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
  });

  $('.slider_area').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });

  $('.slider_col3').slick({
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,

  });
  $('.slider_col3_2').slick({
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,

  });

  $('.new_face_group').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
  });

  $('.mv_img').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
  });
  $('.thum_img').slick({
  slidesToShow: 5,
  asNavFor: '.mv_img',
  autoplay: false,
  focusOnSelect: true,
  arrows: false,
  variableWidth:true,
});

// 画面の高さ取得
var wH = $('body').height(); 
// 各要素の高さ取得
var hH = $('header').height();
var bH = $('.breadcrumb').outerHeight();
var sH = $('.shop_top_area').outerHeight();
var fH = $('footer').outerHeight();
var slickyH = wH - ( hH + bH + sH + fH + 176 ) ;
$('#sticky_continer').css('height', slickyH +'px');

// position: stickyがブラウザで使えるかチェックするための関数
function detectSticky() {
  const div = document.createElement('div');
  div.style.position = 'sticky';
  // position: stickyがブラウザで使えればtrue、使えなければfalseを返す
  return div.style.position.indexOf('sticky') !== -1;
}

// .stickyが指定されている要素に対してposition: stickyを適用させる関数
function callStickyState() {
  // position: stickyを適用させたい要素を引数に指定し、
  // StickyStateをnewしてインスタンスを返す
  return new StickyState(document.querySelectorAll('.shop_main_inner_right'));
}

// もしブラウザでposition: stickyが使えなければ、
// callStickyState関数を呼び出す
if (!detectSticky()) {
  callStickyState();
};


});



